import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from "@nestjs/config";
import {ValidationPipe} from "@nestjs/common";
import {WinstonModule} from "nest-winston";
import {format, transports} from "winston";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger({
      format: format.combine(
          format.timestamp({ format: "isoDateTime" }),
          format.json(),
          format.colorize({ all: true }),
      ),
      transports: [
        new transports.Console(),
      ],
      level: process.env.LOG_LEVEL || 'silly'
    })
  });

  app.useGlobalPipes(new ValidationPipe());

  const config = app.get(ConfigService);
  const port = parseInt(config.get('APP_PORT'), 10);

  await app.listen(port);
}
bootstrap().then(() => {
  console.log(`bootstrap success...`);
});
