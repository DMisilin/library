import { Module, Logger } from '@nestjs/common';

import { NestjsWinstonLoggerModule } from "nestjs-winston-logger";
import { format, transports } from "winston";

@Module({
  imports: [
    NestjsWinstonLoggerModule.forRoot({
      format: format.combine(
          format.timestamp({ format: "isoDateTime" }),
          format.json(),
          format.colorize({ all: true }),
      ),
      transports: [
        new transports.Console(),
      ],
    }),
  ],
  providers: [Logger],
  exports: [Logger],
})
export class LoggerModule {}

