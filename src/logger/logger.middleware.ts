import { Injectable, NestMiddleware, Logger } from '@nestjs/common';

import { Request, Response, NextFunction } from 'express';

@Injectable()
export class AppLoggerMiddleware implements NestMiddleware {
    constructor(
        private readonly logger: Logger,
    ) {
    }

    use(request: Request, response: Response, next: NextFunction): void {
        const { body, baseUrl } = request;
        const message = body ? JSON.stringify(body) : '{}';
        const baseUrlSplit = baseUrl.split('/');
        const method = baseUrlSplit[baseUrlSplit.length - 1];

        this.logger.log(`[API][Request]: method: '${method}', message: '${message}'`);

        response.on('close', () => {
            const { statusCode, statusMessage } = response;
            this.logger.log(`[API][Response]: method: '${method}', statusCode: '${statusCode}', statusMessage: ${statusMessage}`);
        });

        next();
    }
}