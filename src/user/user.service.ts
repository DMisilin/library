import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {User} from "../db/entities/user.entity";

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User, 'library-db')
        private user: Repository<User>
    ) {}

    getUserBy(condition) {
        const select = [
            'user_id AS "userId"',
            'role',
            'name',
            'surname',
            'patronymic',
            'nickname',
            `decode(password, 'hex') AS "password"`,
            'birthday',
            'doc_type AS "docType"',
            'doc_number AS "docNumber"',
        ];

        return this.user.createQueryBuilder()
            .where(condition)
            .select(select)
            .getRawOne();
    }

    async validateUser({nickname, password: pass}) {
        const select = [
            'user_id AS "userId"',
            'role',
            'name',
            'surname',
            'patronymic',
            'nickname',
            `decode(password, 'hex') AS "password"`,
            'birthday',
            'doc_type AS "docType"',
            'doc_number AS "docNumber"',
        ];

        const user =  await this.user.createQueryBuilder()
            .where(`nickname = '${nickname}'`)
            .select(select)
            .getRawOne();

        if (!user) {
            throw new HttpException({statusCode: HttpStatus.NOT_FOUND, message: `User not found`}, HttpStatus.BAD_REQUEST);
        }

        if (user.password.toString() !== pass) {
            throw new HttpException({statusCode: HttpStatus.BAD_REQUEST, message: `Password diff`}, HttpStatus.BAD_REQUEST);
        }

        const {password, ...data} = user;

        return data;
    }

    createUser(params) {
        return this.user.createQueryBuilder()
            .insert()
            .values([params])
            .execute();
    }
}
