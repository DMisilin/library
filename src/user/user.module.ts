import { Module } from '@nestjs/common';
import {DbModule} from "../db/db.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {User} from "../db/entities/user.entity";
import {UserService} from "./user.service";

@Module({
    imports: [
        DbModule,
        TypeOrmModule.forFeature([User], 'library-db'),
    ],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {}
