import {Body, Controller, HttpException, HttpStatus, Inject, Post, UseGuards} from '@nestjs/common';
import {UserService} from "../user/user.service";
import CreateUserDto from "./dto/create-user-dto";
import {BookService} from "../book/book.service";
import CreateBookDto from "./dto/create-book-dto";
import {RawStateEnum} from "../db/types/raw-state-enum";
import GetBooksByDto from "./dto/get-books-by-dto";
import CreateReservationDto from "./dto/create-reservation-dto";
import {BookStatusEnum} from "../db/types/book-status-enum";
import BookManagementDto from "./dto/book-management-dto";
import {OperationEnum} from "../db/types/operation.enum-enum";
import {ReservationEnum} from "../db/types/reservation-enum";
import {AuthGuard} from "@nestjs/passport";

@Controller(process.env.API_PATH)
export class ApiController {
    constructor(
        @Inject(UserService)
        private readonly userService: UserService,
        @Inject(BookService)
        private readonly bookService: BookService,
    ) {}

    @Post('create-user')
    async createUser(@Body() body: CreateUserDto): Promise<any> {
        const {docType, docNumber, password, ...meta} = body;
        await this.userService.createUser({
            doc_number: body.docNumber,
            doc_type: body.docType,
            password: () => `encode('${password}'::bytea, 'hex')`,
            ...meta,
        });

        return {data: {}}
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('get-books-by')
    async getBooks(@Body() body: GetBooksByDto) {
        if (body.fio) {
            const author = await this.bookService.getAuthorBy(`fio = '${body.fio}'`);
            return this.bookService.getBooksBy(`authorToBook.author_id = ${author.author_id}`);
        }

        if (body.title) {
            return this.bookService.getBooksBy(`title LIKE '%${body.title}%'`)
        }

        return this.bookService.getBooks();
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('authors')
    getAuthors() {
        return this.bookService.getAuthors();
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('create-book')
    async createBooks(@Body() body: CreateBookDto) {
        for (const authorId of body.authorIds) {
            const author = await this.bookService.getAuthorBy(`author_id = ${authorId}`);

            if (!author) {
                throw new HttpException({error: `Author with id: ${authorId} not found`}, HttpStatus.BAD_REQUEST);
            }
        }

        const {raw} = await this.bookService.createBook([{
            state: RawStateEnum.ENABLED,
            ...body,
        }]);

        const bookAuthorValues = body.authorIds.reduce((acc, elm) => {
            acc.push({book_id: raw[0].book_id, author_id: elm});
            return acc;
        }, []);

        await this.bookService.setAuthorsToBook(bookAuthorValues);

        return {data: {}};
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('create-reservation')
    async createReservation(@Body() body: CreateReservationDto) {
        const book = await this.bookService.getBooksBy(`book.book_id = ${body.bookId}`);
        if (!book || !book.length || book[0].status !== BookStatusEnum.AVAILABLE) {
            throw new HttpException({error: `Book not found or not available`}, HttpStatus.NOT_FOUND);
        }

        const user = await this.userService.getUserBy(`user_id = ${body.userId}`);
        if (!user) {
            throw new HttpException({message: `User not found`}, HttpStatus.NOT_FOUND);
        }

        return this.bookService.createReservation([{book_id: body.bookId, user_id: body.userId}]);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('book-management')
    async BookManagement(@Body() body: BookManagementDto) {
        const {bookId, userId} = body;
        const book = await this.bookService.getBooksBy(`book.book_id = ${bookId}`);

        if (!book || !book.length) {
            throw new HttpException({error: `Book not found`}, HttpStatus.NOT_FOUND);
        }

        // TODO добавить работу с историей
        if (body.operation === OperationEnum.ISSUE) {
            const condition = `user_id = ${userId} AND book_id = ${bookId}`;
            const reservation = await this.bookService.getReservation(condition);

            if (!reservation) {
                throw new HttpException({error: `Reservation not found`}, HttpStatus.NOT_FOUND);
            }

            if (book[0].status !== BookStatusEnum.AVAILABLE) {
                throw new HttpException({error: `Book not available`}, HttpStatus.BAD_REQUEST);
            }
            await this.bookService.updateReservation({status: ReservationEnum.EXECUTED}, condition);
            await this.bookService.updateBook({status: BookStatusEnum.ISSUED}, `book_id = ${bookId}`);
        }

        if (body.operation === OperationEnum.RETURN) {
            await this.bookService.updateBook({status: BookStatusEnum.AVAILABLE}, `book_id = ${bookId}`);
        }

        return {data: {}};
    }
}
