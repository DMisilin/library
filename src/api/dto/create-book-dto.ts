import {
    ArrayMinSize,
    IsArray,
    IsNotEmpty,
    IsString,
    Length,
} from "class-validator";
import {Type} from "class-transformer";

export default class CreateBookDto {
    @IsNotEmpty()
    @IsString()
    title: string

    @IsNotEmpty()
    @IsString()
    @Length(10, 255)
    description: string

    @IsNotEmpty()
    @IsString()
    genre: string

    @IsNotEmpty()
    @IsArray()
    @Type(() => Number)
    @ArrayMinSize(1)
    authorIds: number[]
}