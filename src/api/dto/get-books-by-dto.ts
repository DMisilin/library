import {IsEmpty, IsNotEmpty, IsOptional, IsString, Length} from "class-validator";

export default class GetBooksByDto {
    @IsOptional()
    @IsString()
    @Length(2, 25)
    fio: string

    @IsOptional()
    @IsString()
    @Length(2, 25)
    title: string
}