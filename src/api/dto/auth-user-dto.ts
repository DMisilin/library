import {IsNotEmpty, IsString, Length} from "class-validator";

export default class AuthUserDto {
    @IsNotEmpty()
    @IsString()
    @Length(2, 25)
    nickname: string

    @IsNotEmpty()
    @IsString()
    @Length(2, 25)
    password: string
}