import {IsDateString, IsNotEmpty, IsString, Length} from "class-validator";

export default class CreateUserDto {
    @IsNotEmpty()
    @IsString()
    role: string

    @IsNotEmpty()
    @IsString()
    @Length(2, 25)
    name: string

    @IsNotEmpty()
    @IsString()
    @Length(2, 25)
    surname: string

    @IsString()
    @Length(2, 25)
    patronymic: string

    @IsNotEmpty()
    @IsString()
    @Length(2, 25)
    nickname: string

    @IsNotEmpty()
    @IsString()
    @Length(2, 25)
    password: string

    @IsNotEmpty()
    @IsDateString()
    birthday: Date

    @IsNotEmpty()
    @IsString()
    docNumber: string

    @IsNotEmpty()
    @IsString()
    docType: string
}