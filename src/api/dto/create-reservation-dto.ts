import {
    IsNotEmpty,
    IsNumber,
} from "class-validator";

export default class CreateReservationDto {
    @IsNumber()
    @IsNotEmpty()
    bookId: number

    @IsNumber()
    @IsNotEmpty()
    userId: number
}