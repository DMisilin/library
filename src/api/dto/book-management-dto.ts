import {
    IsEnum,
    IsNotEmpty, IsNumber,
    IsString,
} from "class-validator";
import {OperationEnum} from "../../db/types/operation.enum-enum";

export default class BookManagementDto {
    @IsNumber()
    @IsNotEmpty()
    bookId: number

    @IsNumber()
    @IsNotEmpty()
    userId: number

    @IsString()
    @IsNotEmpty()
    @IsEnum(OperationEnum)
    operation: string
}