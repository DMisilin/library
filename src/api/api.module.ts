import { Module } from '@nestjs/common';
import { ApiController } from './api.controller';
import {UserModule} from "../user/user.module";
import {BookModule} from "../book/book.module";
import {DbModule} from "../db/db.module";

@Module({
  imports: [UserModule, BookModule, DbModule],
  controllers: [ApiController]
})
export class ApiModule {}
