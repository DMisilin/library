import {Injectable} from '@nestjs/common';
import {InjectRepository} from "@nestjs/typeorm";
import {Book} from "../db/entities/book.entity";
import {Repository} from "typeorm";
import {Author} from "../db/entities/author.entity";
import {AuthorToBook} from "../db/entities/author-to-book.entity";
import {Reservation} from "../db/entities/reservation.entity";

@Injectable()
export class BookService {
    constructor(
        @InjectRepository(Book, 'library-db')
        private book: Repository<Book>,
        @InjectRepository(Author, 'library-db')
        private author: Repository<Author>,
        @InjectRepository(AuthorToBook, 'library-db')
        private authorToBook: Repository<AuthorToBook>,
        @InjectRepository(Reservation, 'library-db')
        private reservation: Repository<Reservation>
    ) {
    }

    getAuthors() {
        return this.author.createQueryBuilder()
            .getMany()
    }

    getAuthorBy(condition) {
        return this.author.createQueryBuilder()
            .where(condition)
            .getOne();
    }

    getBooks() {
        return this.book.createQueryBuilder('book')
            .leftJoinAndSelect('book.bookHistory', 'bookHistory')
            .getMany();
    }

    getBooksBy(value) {
        return this.book.createQueryBuilder('book')
            .leftJoinAndSelect('book.authorToBook', 'authorToBook')
            .where(value)
            .getMany();
    }

    createBook(values): Promise<any> {
        return this.book.createQueryBuilder()
            .insert()
            .values(values)
            .returning('book_id')
            .execute();
    }

    setBookAuthors(values): Promise<any> {
        return this.authorToBook.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    createAuthor(values) {
        return this.book.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    createReservation(values) {
        return this.reservation.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    setAuthorsToBook(values) {
        return this.authorToBook.createQueryBuilder()
            .insert()
            .values(values)
            .execute();
    }

    updateBook(value, condition) {
        return this.book.createQueryBuilder()
            .update()
            .set(value)
            .where(condition)
            .execute();
    }

    getReservation(condition) {
        return this.reservation.createQueryBuilder()
            .where(condition)
            .getOne();
    }

    updateReservation(value, condition) {
        return this.reservation.createQueryBuilder()
            .update()
            .set(value)
            .where(condition)
            .execute();
    }
}
