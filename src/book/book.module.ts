import {Module} from '@nestjs/common';
import {DbModule} from "../db/db.module";
import {BookService} from "./book.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Author} from "../db/entities/author.entity";
import {AuthorToBook} from "../db/entities/author-to-book.entity";
import {Book} from "../db/entities/book.entity";
import {Reservation} from "../db/entities/reservation.entity";

// TODO переименовать модуль во что-то глобальное, тк делает она операции не только с книгами

@Module({
    imports: [
        DbModule,
        TypeOrmModule.forFeature([
            Book,
            Author,
            AuthorToBook,
            Reservation,
        ], 'library-db'),
    ],
    providers: [BookService],
    exports: [BookService]
})
export class BookModule {
}
