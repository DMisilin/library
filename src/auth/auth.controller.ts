import {Body, Controller, Inject, Post} from '@nestjs/common';
import {UserService} from "../user/user.service";
import AuthUserDto from "../api/dto/auth-user-dto";
import {AuthService} from "./auth.service";

@Controller(process.env.API_PATH)
export class AuthController {
    constructor(
        @Inject(UserService)
        private readonly userService: UserService,
        @Inject(AuthService)
        private readonly authService: AuthService,
    ) {}

    @Post('login')
    async createUser(@Body() body: AuthUserDto): Promise<any> {
        const {role} = await this.userService.validateUser(body);

        const token = await this.authService.generateJwt({
            role,
            ...body,
        });

        return {data: {token}}
    }
}
