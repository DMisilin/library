import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
    handleRequest(err: any, user: any, info: any, context: any, status: any) {
        if (process.env.JWT_VALIDATION_IS_ENABLED && process.env.JWT_VALIDATION_IS_ENABLED === 'true') {
            return super.handleRequest(err, user, info, context, status);
        }
    }
}