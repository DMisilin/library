import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {ScheduleModule} from "@nestjs/schedule";
import {ConfigModule} from "@nestjs/config";
import {DbModule} from './db/db.module';
import {UserModule} from './user/user.module';
import {BookModule} from './book/book.module';
import {ApiModule} from './api/api.module';
import {AuthModule} from './auth/auth.module';
import {AppLoggerMiddleware} from "./logger/logger.middleware";
import {LoggerModule} from "./logger/logger.module";

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: '.env',
            isGlobal: true,
        }),
        ScheduleModule.forRoot(),
        DbModule,
        UserModule,
        BookModule,
        ApiModule,
        AuthModule,
        LoggerModule,
    ]
})

export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void {
        consumer.apply(AppLoggerMiddleware).forRoutes('*');
    }
}
