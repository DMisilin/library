import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {RawStateEnum} from "../types/raw-state-enum";
import {Author} from "./author.entity";
import {Book} from "./book.entity";

@Entity({schema: process.env.DB_PG_RACE_DATABASE_SCHEMA})
export class AuthorToBook {
    @PrimaryGeneratedColumn({comment: 'Ид.'})
    author_to_book_id: number

    @Column({comment: 'Ид. книги', nullable: false})
    book_id: number

    @Column({comment: 'Ид. автора', nullable: false})
    author_id: number

    @Column({type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP", comment: 'Время создания'})
    create_dt: Date

    @Column({
        type: 'enum',
        enum: RawStateEnum,
        default: RawStateEnum.ENABLED,
        comment: 'Статус записи в таблице'
    })
    state: string

    @ManyToOne(() => Author)
    @JoinColumn({name: 'author_id'})
    author: Author

    @ManyToOne(() => Book)
    @JoinColumn({name: 'book_id'})
    book: Book
}
