import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Book} from "./book.entity";
import {ReservationEnum} from "../types/reservation-enum";
import {User} from "./user.entity";

@Entity({schema: process.env.DB_PG_RACE_DATABASE_SCHEMA})
export class Reservation {
    @PrimaryGeneratedColumn({comment: 'Ид.'})
    reservation_id: number

    @Column({comment: 'Ид. книги'})
    book_id: number

    @Column({comment: 'Ид. пользователя'})
    user_id: number

    @Column({type: "timestamp with time zone", default: () => "CURRENT_TIMESTAMP", comment: 'Время создания'})
    create_dt: Date

    @Column({type: "timestamp with time zone", nullable: true, comment: 'Время последнего изменения'})
    modify_dt: Date

    @Column({
        type: 'enum',
        enum: ReservationEnum,
        default: ReservationEnum.CREATED,
        comment: 'Статус запроса'
    })
    status: string

    @ManyToOne(() => Book)
    @JoinColumn({name: 'book_id'})
    book: Book

    @ManyToOne(() => User)
    @JoinColumn({name: 'user_id'})
    user: User
}
