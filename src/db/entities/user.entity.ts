import {Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique} from "typeorm";
import {Reservation} from "./reservation.entity";
import {UserTypesEnum} from "../types/user-type-enum";

@Entity({ schema: process.env.DB_PG_RACE_DATABASE_SCHEMA })
@Unique(['nickname'])
export class User {
    @PrimaryGeneratedColumn({comment: 'Ид.'})
    user_id: number

    // TODO по хорошему тут должно быть id на словарь с ролями, но для быстро и чтобы не усложнять - сразу наименование
    @Column({
        type: 'enum',
        enum: UserTypesEnum,
        comment: 'Роль'
    })
    role: string

    @Column({comment: 'Имя'})
    name: string

    @Column({comment: 'Фамилия'})
    surname: string

    @Column({comment: 'Отчество', nullable: true})
    patronymic: string

    @Column({comment: 'Никнейм'})
    nickname: string

    @Column({comment: 'Парооль'})
    password: string

    @Column({type: 'timestamp with time zone', comment: 'День рождения'})
    birthday: Date

    @Column({comment: 'Серия и номер документа'})
    doc_number: string

    // TODO по хорошему тут должно быть id на словарь с типами документов, но для быстро и чтобы не усложнять - сразу наименование
    @Column({comment: 'Типа документа'})
    doc_type: string

    @OneToMany(() => Reservation, (Reservation) => Reservation.book)
    reservation: Reservation[]
}
