import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import {RawStateEnum} from "../types/raw-state-enum";
import {AuthorToBook} from "./author-to-book.entity";

@Entity({ schema: process.env.DB_PG_RACE_DATABASE_SCHEMA })
export class Author {
    @PrimaryGeneratedColumn({comment: 'Ид.'})
    author_id: number

    @Column({comment: 'ФИО', nullable: false})
    fio: string

    @Column({type: 'timestamp with time zone', comment: 'День рождения'})
    birthday: Date

    @Column({
        type: 'enum',
        enum: RawStateEnum,
        default: RawStateEnum.ENABLED,
        comment: 'Состояние записи в таблице'
    })
    state: string

    @OneToMany(() => AuthorToBook, (AuthorToBook) => AuthorToBook.author)
    authorToBook: AuthorToBook[]
}
