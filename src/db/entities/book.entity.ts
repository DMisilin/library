import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import {BookStatusEnum} from "../types/book-status-enum";
import {AuthorToBook} from "./author-to-book.entity";
import {Reservation} from "./reservation.entity";

@Entity({ schema: process.env.DB_PG_RACE_DATABASE_SCHEMA })
export class Book {
    @PrimaryGeneratedColumn({comment: 'Ид.'})
    book_id: number

    @Column({comment: 'Имя', nullable: false})
    title: string

    @Column({comment: 'Фамилия', nullable: false})
    description: string

    @Column({comment: 'Ид. жанра', nullable: false})
    genre: string

    @Column({
        type: 'enum',
        enum: BookStatusEnum,
        default: BookStatusEnum.AVAILABLE,
        comment: 'Состояние записи в таблице'
    })
    status: string

    @OneToMany(() => AuthorToBook, (AuthorToBook) => AuthorToBook.book)
    authorToBook: AuthorToBook[]

    @OneToMany(() => Reservation, (Reservation) => Reservation.book)
    reservation: Reservation[]
}
