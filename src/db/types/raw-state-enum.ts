export enum RawStateEnum {
    ENABLED = 'enabled',
    DISABLED = 'disabled'
}