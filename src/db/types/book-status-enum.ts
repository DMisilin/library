export enum BookStatusEnum {
    ISSUED = 'issued',
    AVAILABLE = 'available',
    BOOKED = 'booked',
}