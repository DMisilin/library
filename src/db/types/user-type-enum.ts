export enum UserTypesEnum {
    MEMBER = 'member',
    ADMINISTRATOR = 'administrator'
}