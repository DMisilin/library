export enum ReservationEnum {
    CREATED = 'created',
    EXECUTED = 'executed',
    REJECTED = 'rejected'
}