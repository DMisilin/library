import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule } from "@nestjs/config";

console.log([__dirname + '/entities/*.entity{.ts,.js}'])

@Module({
    imports: [
        ConfigModule.forRoot({envFilePath: '.env'}),
        TypeOrmModule.forRoot({
            name: 'library-db',
            autoLoadEntities: true,
            type: 'postgres',
            host: process.env.DB_PG_LIBRARY_HOST,
            port: parseInt(process.env.DB_PG_LIBRARY_PORT, 10) || 5432,
            username: process.env.DB_PG_LIBRARY_USERNAME,
            password: process.env.DB_PG_LIBRARY_PASSWORD,
            database: process.env.DB_PG_LIBRARY_DATABASE,
            schema: process.env.DB_PG_LIBRARY_SCHEMA,
            // entities: [__dirname + '/entities/*.entity{.ts,.js}'],
            // migrations: [__dirname + '/migrations/*{.ts,.js}'],
            // @ts-ignore
            // cli: { migrationsDir: __dirname + '/migrations' },
            // migrationsTableName: 'migrations',
            // migrationsRun: true,
            synchronize: true,
            logging: JSON.parse(process.env.DB_PG_LIBRARY_QUERY_LOG || 'false')
        })
    ],
    exports: [TypeOrmModule]
})
export class DbModule {}
