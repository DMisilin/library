import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";
import { ConfigModule } from "@nestjs/config";

ConfigModule.forRoot({envFilePath: '../.env'})

const config: PostgresConnectionOptions = {
    name: 'library-db',
    type: 'postgres',
    host: process.env.DB_PG_LIBRARY_HOST,
    port: parseInt(process.env.DB_PG_LIBRARY_PORT, 10) || 5432,
    username: process.env.DB_PG_LIBRARY_USERNAME,
    password: process.env.DB_PG_LIBRARY_PASSWORD,
    database: process.env.DB_PG_LIBRARY_DATABASE,
    schema: process.env.DB_PG_LIBRARY_SCHEMA,
    entities: [__dirname + '/entities/*.entity{.ts,.js}'],
    migrations: [__dirname + '/migrations/*{.ts,.js}'],
    // @ts-ignore
    cli: { migrationsDir: __dirname + '/migrations' },
    migrationsTableName: 'migrations',
    migrationsRun: true,
    synchronize: false,
}

export default config;
