## API

Программный интерфейс приложения

### TOC

- [API](#api)
  - [TOC](#toc)
  - [AMQP](#amqp)
    - [Methods](#methods)
      - [login](#login)
        - [Description](#description)
        - [Input](#input)
        - [Output](#output)
      - [create-user](#create-user)
        - [Description](#description-1)
        - [Input](#input-1)
        - [Output](#output-1)

## AMQP

Взаимодействие по AMQP протоколу

#### Methods

Обработчики


##### login

Метод для аутендификации и получения токена для доступа к api

###### Description

Описание

- Проверяет наличие пользователя и в таблице `user`
- Если пользователь найден, то возвращается токен

###### Input

Принимает параметры

- `nickname` - *string* - Никнейм / логин
- `password` - *string* - Пароль

###### Output

Возвращает объект data с параметрами:

- `token` - *string* - Токен



##### create-user

Создание пользователя

###### Description

Описание

- Проверяет наличие пользователя и в таблице `user`
- Если пользователь найден, то возвращается токен

###### Input

Принимает параметры

- `role` - *string* - Роль
- `name` - *string* - Имя
- `surname` - *string* - Фамилия
- `patronymic` - *string* - Отчество
- `birthday` - *string* - Дата рождения
- `docNumber` - *string* - Номер документа
- `docType` - *string* - Тип документа
- `nickname` - *string* - Никнейм / логин
- `password` - *string* - Пароль

###### Output

Возвращает объект data с параметрами:

- `role` - *string* - Роль
- `name` - *string* - Имя
- `surname` - *string* - Фамилия
- `patronymic` - *string* - Отчество
- `birthday` - *string* - Дата рождения
- `docNumber` - *string* - Номер документа
- `docType` - *string* - Тип документа
- `nickname` - *string* - Никнейм / логин
- `password` - *string* - Пароль
